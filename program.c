#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <CL/cl.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

const char* loadFromFile(const char* fileName) {
    FILE* file = fopen(fileName, "r");

    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    rewind(file);

    char* fileContents = malloc(fileSize + 1);
    fread(fileContents, fileSize, 1, file);
    fileContents[fileSize] = '\0';

    return fileContents;
}

int main(int argc, char* argv[]) {
    cl_platform_id platforms[10];
    int numPlatforms;

    clGetPlatformIDs(10, platforms, &numPlatforms);

    cl_platform_id platform = platforms[0];

    cl_device_id devices[10];
    int numDevices;

    clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 10, devices, &numDevices);

    cl_context context = clCreateContext(NULL, 1, devices, NULL, NULL, NULL);

    const char* sourceCode = loadFromFile("kernel.cl");
    const size_t sourceCodeLength = strlen(sourceCode);
    cl_program program = clCreateProgramWithSource(context, 1, &sourceCode,
            &sourceCodeLength, NULL);

    clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    cl_command_queue commandQueue = clCreateCommandQueueWithProperties(context,
            devices[0], 0, NULL);

    int width, height;
    int channels;

    unsigned char* sourceImageData = stbi_load("lena.png",
            &width, &height,
            &channels,
            4);

    cl_image_desc* imageDescriptor = calloc(sizeof(cl_image_desc), 1);

    imageDescriptor->image_type = CL_MEM_OBJECT_IMAGE2D;
    imageDescriptor->image_width = width;
    imageDescriptor->image_height = height;

    cl_image_format sourceImageFormat;
    cl_image_format destImageFormat;

    sourceImageFormat.image_channel_order = CL_RGBA;
    sourceImageFormat.image_channel_data_type = CL_UNORM_INT8;

    destImageFormat.image_channel_order = CL_R;
    destImageFormat.image_channel_data_type = CL_UNORM_INT8;

    cl_mem sourceImageBuffer = clCreateImage(context,
            CL_MEM_READ_ONLY,
            &sourceImageFormat,
            imageDescriptor,
            NULL,
            NULL);

    cl_mem destImageBuffer = clCreateImage(context,
            CL_MEM_WRITE_ONLY,
            &destImageFormat,
            imageDescriptor,
            NULL,
            NULL);

    free((void*)imageDescriptor);

    size_t sourceImageOffset[] = { 0, 0, 0 };
    size_t sourceImageRegion[] = { width, height, 1 };

    clEnqueueWriteImage(commandQueue,
            sourceImageBuffer,
            CL_FALSE,
            sourceImageOffset,
            sourceImageRegion,
            0, 0,
            sourceImageData,
            0,
            NULL,
            NULL);

    cl_kernel kernel = clCreateKernel(program, "grayscaleFilter", NULL);

    clSetKernelArg(kernel, 0, sizeof(sourceImageBuffer), &sourceImageBuffer);
    clSetKernelArg(kernel, 1, sizeof(destImageBuffer), &destImageBuffer);

    size_t globalWorkSize[] = { width, height };

    clEnqueueNDRangeKernel(commandQueue,
            kernel,
            2,
            NULL,
            globalWorkSize,
            NULL,
            0,
            NULL,
            NULL);

    unsigned char* destImageData = malloc(width * height);

    size_t destImageOffset[] = { 0, 0, 0 };
    size_t destImageRegion[] = { width, height, 1 };

    clEnqueueReadImage(commandQueue,
            destImageBuffer,
            CL_TRUE,
            destImageOffset,
            destImageRegion,
            0, 0,
            destImageData,
            0,
            NULL,
            NULL);

    stbi_write_png("lena_gray.png",
            width, height,
            1,
            destImageData,
            width);

    stbi_image_free(sourceImageData);
    free(destImageData);

    clReleaseKernel(kernel);

    clReleaseMemObject(sourceImageBuffer);
    clReleaseMemObject(destImageBuffer);

    clReleaseProgram(program);
    clReleaseContext(context);

    free((void*)sourceCode);

    return 0;
}
