__kernel void grayscaleFilter(
        __read_only image2d_t sourceImage,
        __write_only image2d_t destImage) {
    int2 coordinate = (int2)(get_global_id(0), get_global_id(1));

    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE
            | CLK_ADDRESS_CLAMP_TO_EDGE
            | CLK_FILTER_NEAREST;

    float4 colorPixel = read_imagef(sourceImage, sampler, coordinate);
    float4 colorWeights = (float4)(0.2126, 0.7152, 0.0722, 0);

    float grayscaleValue = dot(colorPixel, colorWeights);

    float4 grayscalePixel = (float4)(grayscaleValue, 0, 0, 1);

    write_imagef(destImage, coordinate, grayscalePixel);
}
